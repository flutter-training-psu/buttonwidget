import 'package:flutter/material.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Buttons example',

      // to hide debug banner
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.green,
      ),
      home: ButtonsExample(),
    );
  }
}

// list all the buttons
class ButtonsExample extends StatelessWidget {
// const ButtonsExample ({Key ?key}) : super(key:key);
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey();

  ButtonsExample({Key? key}) : super(key: key);

// ignore: avoid_print
// assign actions to
// all the listed buttons
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        key: scaffoldKey,
        appBar: AppBar(
          title: const Text('Buttons example'),
        ),
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Center(
                child: TextButton(
                  onPressed: (() {
                    ScaffoldMessenger.of(context).showSnackBar(
                      const SnackBar(
                        content: Text("Text / Flat Button"),
                        duration: Duration(seconds: 1),
                      ),
                    );
                  }),
                  child: const Text('Text Button'),
                ),
              ),
              // FlatButton is Deprecated and will be removed in the future.
              // We recommend using TextButton instead.
              // FlatButton(
              // minWidth: MediaQuery.of(context).size.width,
              // onPressed: () {
              //	 ScaffoldMessenger.of(context).showSnackBar(
              //	 const SnackBar(
              //		 content: Text("Text / Flat Button"),
              //		 duration: Duration(seconds: 1),
              //	 ),
              //	 );
              // },
              // child: const Text('Flat Button'),
              // ),
              ElevatedButton(
                onPressed: () {
                  ScaffoldMessenger.of(context).showSnackBar(
                    const SnackBar(
                      content: Text("Elevated / Raised Button"),
                      duration: Duration(seconds: 1),
                    ),
                  );
                },
                child: const Text('Elevated Button'),
              ),
              // RaisedButton is Deprecated and will be removed in the next release.
              // Use ElevatedButton instead.
              // RaisedButton(
              // onPressed: () {
              //	 ScaffoldMessenger.of(context).showSnackBar(
              //	 const SnackBar(
              //		 content: Text("Elevated / Raised Button"),
              //		 duration: Duration(seconds: 1),
              //	 ),
              //	 );
              // },
              // child: const Text('Raised Button'),
              // ),
              OutlinedButton(
                onPressed: () {
                  ScaffoldMessenger.of(context).showSnackBar(
                    const SnackBar(
                      content: Text("Outline / Outlined Button"),
                      duration: Duration(seconds: 1),
                    ),
                  );
                },
                child: const Text('Outline Button'),
              ),
              OutlinedButton(
                onPressed: () {
                  ScaffoldMessenger.of(context).showSnackBar(
                    const SnackBar(
                      content: Text("Outline / Outlined Button"),
                      duration: Duration(seconds: 1),
                    ),
                  );
                },
                child: const Text('Outlined Button'),
              ),
              IconButton(
                icon: const Icon(Icons.star),
                onPressed: () {
                  ScaffoldMessenger.of(context).showSnackBar(
                    const SnackBar(
                      content: Text("Icon Button"),
                      duration: Duration(seconds: 1),
                    ),
                  );
                },
              ),
              FloatingActionButton.extended(
                onPressed: () {
                  ScaffoldMessenger.of(context).showSnackBar(
                    const SnackBar(
                      content: Text("Floating Action Button"),
                      duration: Duration(seconds: 1),
                    ),
                  );
                },
                label: const Text('Floating Action Button'),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
